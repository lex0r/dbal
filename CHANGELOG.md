# Soong\DBAL

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

soong\dbal provides basic DBAL integration with the Soong data migration framework. At the moment, the implementation is very basic, for testing and demonstration purposes - it needs to be fleshed out to provide flexible extraction/loading/keymap services.

## Install

Via Composer

``` bash
$ composer require soong/dbal
```

## Usage

``` php
$dbal = new Soong\DBAL\Loader\DBALLoader($configuration);
$dbal($data);
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

### Todo
* Complete test coverage
* Customize separator
* Customize output file/stream

## Security

If you discover any security related issues, please email `soong@virtuoso-performance.com` instead of using the issue tracker.

## Credits

- [Mike Ryan][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/soong/dbal.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/soong/dbal/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/soong/dbal.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/soong/dbal.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/soong/dbal.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/soong/dbal
[link-travis]: https://travis-ci.org/soong/dbal
[link-scrutinizer]: https://scrutinizer-ci.com/g/soong/dbal/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/soong/dbal
[link-downloads]: https://packagist.org/packages/soong/dbal
[link-author]: https://gitlab.com/mikeryan776
[link-contributors]: ../../contributors
