<?php
declare(strict_types=1);

namespace Soong\DBAL\Loader;

use Doctrine\DBAL\DBALException;
use Soong\Contracts\Data\RecordPayload;
use Soong\Contracts\Exception\LoaderException;
use Soong\DBAL\DBAL;
use Soong\Loader\LoaderBase;

/**
 * Loader for DBAL SQL tables.
 */
class DBALLoader extends LoaderBase
{

    use DBAL;

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['connection'] = [
            'required' => true,
            'allowed_types' => 'array',
        ];
        $options['table'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(RecordPayload $payload) : RecordPayload
    {
        try {
            $connection = $this->connection();
        } catch (DBALException $e) {
            throw new LoaderException('Unable to connect to database.', 0, $e);
        }
        try {
            $destinationRecord = $payload->getDestinationRecord();
            $table = $this->getConfigurationValue('table');
            $connection->insert(
                $table,
                $destinationRecord->toArray()
            );
            $id = $connection->lastInsertId();
            if ($id) {
                $keyKeys = array_keys($this->getKeyProperties());
                $destinationRecord->setPropertyValue(reset($keyKeys), $id);
            }
            return $payload;
        } catch (DBALException $e) {
            throw new LoaderException("Unable to insert into $table.", 0, $e);
        }
    }

    /**
     * @inheritdoc
     */
    public function delete(array $key) : void
    {
        try {
            $connection = $this->connection();
        } catch (DBALException $e) {
            throw new LoaderException('Unable to connect to database.', 0, $e);
        }
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->delete($this->getConfigurationValue('table'));
        $counter = 0;
        foreach ($key as $name => $value) {
            $queryBuilder->andWhere("$name = ?");
            $queryBuilder->setParameter($counter, $value);
            $counter++;
        }
        $queryBuilder->execute();
        $connection->close();
        $this->connection = null;
    }
}
