<?php
declare(strict_types=1);

namespace Soong\DBAL\Loader;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Soong\Contracts\Data\Record;
use Soong\Contracts\Data\RecordPayload;
use Soong\Contracts\Exception\LoaderException;
use Soong\DBAL\Loader\DBALLoader;

/**
 * Similar to DBALLoader but this implementation doesn't attempt to create a new record if one already exists and
 * instead creates new and updates existing records.
 */
class DBALMergeLoader extends DBALLoader
{

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['disable_rollback'] = [
            'required' => false,
            'default_value' => false,
            'allowed_types' => 'boolean',
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(RecordPayload $payload) : RecordPayload
    {
        try {
            $connection = $this->connection();
        } catch (DBALException $e) {
            throw new LoaderException('Unable to connect to database.', 0, $e);
        }

        try {
            $destinationRecord = $payload->getDestinationRecord();
            $table = $this->getConfigurationValue('table');
            $keysValues = $this->getKeysValues($destinationRecord);
            $keysTypes = $this->getKeysTypes();
            $existingRecord = $this->getRecordByKeys($connection, $table, $keysValues, $keysTypes);
            if (!$existingRecord) {
                $connection->insert(
                    $table,
                    $destinationRecord->toArray()
                );
                // If only one key is defined on the loader it's assumed to be the auto-increment ID and needs to be
                // set on the destination record for the key map to work. Otherwise it's transformers responsibility
                // to set all the keys that may be needed by the key map.
                if (count($keysValues) == 1) {
                    $id = $connection->lastInsertId();
                    if ($id) {
                        $keyKeys = array_keys($keysValues);
                        $destinationRecord->setPropertyValue($keyKeys[0], $id);
                    }
                }
            } else {
                $connection->update($table, $destinationRecord->toArray(), $keysValues);
            }
            return $payload;
        } catch (DBALException $e) {
            throw new LoaderException("Unable to insert into $table.", 0, $e);
        }
    }

    /**
     * @inheritdoc
     */
    public function delete(array $key) : void
    {
        /**
         * TODO: this feature has to be expanded to cases when the parent class is used so that it works regardless
         * of the type of loader.
         */
        if ($this->getConfigurationValue('disable_rollback')) {
            throw new LoaderException("Rollback is not possible for this task");
        }
        parent::delete($key);
    }

    /**
     * @param Record $destinationRecord
     * @return array
     */
    private function getKeysValues(Record $destinationRecord)
    {
        $result = [];
        $keys = array_keys($this->getKeyProperties());
        foreach ($keys as $key) {
            $result[$key] = $destinationRecord->getPropertyValue($key);
        }
        return $result;
    }

    /**
     * @param Connection $connection
     * @param string $table
     * @param array $keysValues
     * @param array $keysTypes
     * @return false|mixed[]
     * @throws \Doctrine\DBAL\Exception
     */
    private function getRecordByKeys(Connection $connection, string $table, array $keysValues, array $keysTypes)
    {
        $keys = array_keys($keysValues);
        $values = array_values($keysValues);
        $types = array_values($keysTypes);
        $sql = "SELECT * FROM $table WHERE ";
        $sqlWhere = [];
        foreach ($keys as $key) {
            $sqlWhere[] = "$key = ?";
        }
        $sql .= implode(' AND ', $sqlWhere);
        return $connection->fetchAssociative($sql, $values, $types);
    }

    /**
     * @return array
     */
    private function getKeysTypes()
    {
        $result = [];
        $keyProperties = $this->getKeyProperties();
        foreach ($keyProperties as $key => $property) {
            if (!in_array($property['type'], ['integer', 'string'])) {
                throw new LoaderException("Invalid PK type {$property['type']}.");
            }
            $result[$key] = $property['type'] == 'integer' ? \PDO::PARAM_INT : \PDO::PARAM_STR;
        }
        return $result;
    }
}
