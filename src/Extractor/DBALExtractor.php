<?php
declare(strict_types=1);

namespace Soong\DBAL\Extractor;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Soong\Contracts\Exception\ExtractorException;
use Soong\DBAL\DBAL;
use Soong\Extractor\CountableExtractorBase;

/**
 * Extractor for DBAL SQL queries.
 */
class DBALExtractor extends CountableExtractorBase
{
    private const DEFAULT_CHUNK_SIZE = 1000;

    use DBAL;

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['connection'] = [
            'required' => true,
            'allowed_types' => 'array',
        ];
        $options['query'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        $options['count_query'] = [
            'required' => false,
            'allowed_types' => 'string',
        ];
        $options['chunk_size'] = [
            'required' => false,
            'allowed_types' => 'integer',
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function extractAll() : iterable
    {
        try {
            $connection = $this->connection();
        } catch (DBALException $e) {
            throw new ExtractorException('Unable to connect to database.', 0, $e);
        }

        $useCountQuery = $this->getConfigurationValue('count_query');
        if ($useCountQuery) {
            $query = $this->getConfigurationValue('query');
            // @todo Think if we should support chunked queries when a LIMIT is already present.
            if (preg_match( '/limit\s\d/i', $query) === 1) {
                throw new ExtractorException("Count query can't be used if the main query has a LIMIT clause");
            }
            yield from $this->queryInChunks($connection);
            return;
        }

        try {
            // @todo: don't accept raw SQL from configuration
            /** @var \Doctrine\DBAL\Driver\Statement $statement */
            $query = $this->getConfigurationValue('query');
            $statement = $connection->executeQuery($query);
        } catch (DBALException $e) {
            throw new ExtractorException("Error executing query '$query'.", 0, $e);
        }
        while ($row = $statement->fetch(FetchMode::ASSOCIATIVE)) {
            yield $this->getConfigurationValue('record_factory')->create($row);
        }
        $this->connection->close();
        unset($this->connection);
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        // @todo: Identify properties from the query.
        return parent::getProperties();
    }

    /**
     * Override in order to try to use a count query for performance reasons.
     */
    protected function getUncachedCount()
    {
        $query = $this->getConfigurationValue('count_query');
        if (!$query) {
            return parent::getUncachedCount();
        }
        try {
            $connection = $this->connection();
        } catch (DBALException $e) {
            throw new ExtractorException('Unable to connect to database.', 0, $e);
        }
        try {
            $statement = $connection->executeQuery($query);
        } catch (DBALException $e) {
            throw new ExtractorException("Error executing query '$query'.", 0, $e);
        }
        return $statement->fetch(FetchMode::NUMERIC)[0];
    }

    /**
     * Break the query and fetch data in chunks to improve performance.
     */
    protected function queryInChunks($connection) : iterable
    {
        $chunkSize = $this->getConfigurationValue('chunk_size') ?? self::DEFAULT_CHUNK_SIZE;
        $offset = 0;
        do {
            try {
                // @todo: don't accept raw SQL from configuration
                /** @var \Doctrine\DBAL\Driver\Statement $statement */
                $query = $this->getConfigurationValue('query');
                $query = "$query LIMIT $offset, $chunkSize";
                $statement = $connection->executeQuery($query);
            } catch (DBALException $e) {
                throw new ExtractorException("Error executing query '$query'.", 0, $e);
            }
            $count = 0;
            while ($row = $statement->fetch(FetchMode::ASSOCIATIVE)) {
                $count++;
                yield $this->getConfigurationValue('record_factory')->create($row);
            }
            $offset += $chunkSize;
        } while ($count === $chunkSize);

        $this->connection->close();
        unset($this->connection);
    }
}
